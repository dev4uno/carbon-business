<?php
declare(strict_types=1);

namespace C4uno\CarbonBusiness;

use C4uno\CarbonBusiness\Contracts\HolidayRepository;
use Carbon\Carbon;
use InvalidArgumentException;
use Mockery;
use PHPUnit\Framework\TestCase;

class BusinessDateManagerTest extends TestCase
{
    private CarbonBusiness $manager;
    private $holidayRepository;

    public function testIsBusiness()
    {
        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(false);
        $carbonBusiness = Carbon::today();
        $this->assertTrue($this->manager->isBusinessDay($carbonBusiness));

        $carbonBusiness = Carbon::today()->addDays(6);// Weekend
        $this->assertFalse($this->manager->isBusinessDay($carbonBusiness));

        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(true);
        $carbonBusiness->addDays(1); // Next monday
        $this->assertFalse($this->manager->isBusinessDay($carbonBusiness));
    }

    public function testNextBusinessDay()
    {
        $nextBusinessDay = $this->manager->subBusinessDays(Carbon::today(), 0)->toDateString();
        $this->assertEquals($nextBusinessDay, Carbon::today()->toDateString());

        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(false);
        $nextBusinessDay = $this->manager->addBusinessDays(Carbon::today(), 1)->toDateString();
        $this->assertEquals($nextBusinessDay, Carbon::now()->addDay()->toDateString());

        // Two day ahead
        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(true);
        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(false);
        $nextBusinessDay = $this->manager->addBusinessDays(Carbon::today(), 1)->toDateString();
        $this->assertEquals($nextBusinessDay, Carbon::now()->addDays(2)->toDateString());
    }

    public function testPreviousBusinessDay()
    {
        $previousDate = $this->manager->subBusinessDays(Carbon::today(), 0)->toDateString();
        $this->assertEquals($previousDate, Carbon::today()->toDateString());

        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(false);
        $previousDate = $this->manager->subBusinessDays(Carbon::today(), 1)->toDateString();
        $this->assertEquals($previousDate, Carbon::now()->subDays(3)->toDateString());

        // Two day before
        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(true);
        $this->holidayRepository
            ->shouldReceive('hasDate')
            ->once()
            ->andReturn(false);
        $previousDate = $this->manager->subBusinessDays(Carbon::today(), 1)->toDateString();
        $this->assertEquals($previousDate, Carbon::now()->subDays(4)->toDateString());
    }

    /**
     * @dataProvider inWorkingHoursProvider
     * @param int     $hour
     * @param int     $minute
     * @param boolean $expected
     */
    public function testInWorkingHours($hour, $minute, $expected)
    {
        $workingHours = Carbon::now();
        $workingHours->hour = $hour;
        $workingHours->minute = $minute;

        $this->assertEquals($expected, $this->manager->inWorkingHours($workingHours));
    }

    /**
     * @param int $hour
     * @param int $minute
     * @dataProvider setWorkingHoursProvider
     */
    public function testSetWorkingHoursStart($hour, $minute)
    {
        $this->expectException(InvalidArgumentException::class);
        $this->manager->setWorkingHoursStart($hour, $minute);
    }

    public function inWorkingHoursProvider()
    {
        return [
            [7, 59, false],
            [8, 0, true],
            [17, 59, true],
            [18, 0, false],
        ];
    }

    public function setWorkingHoursProvider()
    {
        return [
            [24, 0],
            [-1, 0],
            [23, 60],
            [0, -1],
        ];
    }

    protected function setUp(): void
    {
        $now = Carbon::create(2018, 8, 6);// Monday
        Carbon::setTestNow($now);

        $this->holidayRepository = Mockery::mock('repo', HolidayRepository::class);
        $this->manager = new CarbonBusiness($this->holidayRepository);
        $this->manager->setWorkingHoursStart(8, 0);
        $this->manager->setWorkingHoursEnd(18, 0);
    }

    protected function tearDown(): void
    {
        Carbon::setTestNow();
        parent::tearDown();
    }
}
