<?php
declare(strict_types=1);

namespace C4uno\CarbonBusiness\Repositories;

use C4uno\CarbonBusiness\Models\Holiday;
use C4uno\CarbonBusiness\Providers\LaravelServiceProvider;
use Carbon\Carbon;
use Orchestra\Testbench\TestCase;

class EloquentHolidayRepositoryTest extends TestCase
{
    public function testHasDate()
    {
        $repository = app(EloquentHolidayRepository::class);
        $result = $repository->hasDate(now());
        $this->assertFalse($result);

        $holiday = new Holiday();
        $holiday->holiday = now();
        $holiday->description = 'A new holiday';
        $holiday->save();

        $date = Carbon::today();
        $date->addHours(rand(1, 23));
        $date->addMinutes(rand(1, 59));
        $date->addSeconds(rand(1, 59));

        $result = $repository->hasDate($date);
        $this->assertTrue($result);
    }

    protected function setUp(): void
    {
        parent::setUp();

        Carbon::setTestNow('2020-12-01 20:20:20');
        $this->artisan('migrate');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelServiceProvider::class,
        ];
    }
}
