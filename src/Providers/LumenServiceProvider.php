<?php

namespace C4uno\CarbonBusiness\Providers;

/**
 * Class CarbonBusinessServiceProvider
 * @package C4uno\CarbonBusiness
 */
class LumenServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        $version = $this->getVersion();

        if ($version >= 5.3) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        } else {
            $this->publishes([
                __DIR__.'/../../database/migrations/' => database_path('migrations'),
            ], 'migrations');
        }
    }
}
