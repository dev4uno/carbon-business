<?php

namespace C4uno\CarbonBusiness\Providers;

use C4uno\CarbonBusiness\CarbonBusiness;
use C4uno\CarbonBusiness\Contracts\HolidayRepository;
use C4uno\CarbonBusiness\Repositories\EloquentHolidayRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class CarbonBusinessServiceProvider
 *
 * @package C4uno\CarbonBusiness
 */
class BaseServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $version = $this->getVersion();

        $this->publishes([
            __DIR__.'/../../config/carbon-business.php' => config_path('carbon-business.php'),
        ], 'config');

        if ($version >= 5.3) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        } else {
            $this->publishes([
                __DIR__.'/../../database/migrations/' => database_path('migrations'),
            ], 'migrations');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/carbon-business.php', 'carbon-business');

        $this->app->singleton(HolidayRepository::class, EloquentHolidayRepository::class);

        $startHour = explode(':', config('carbon-business.start_working_hour'));
        $endHour = explode(':', config('carbon-business.end_working_hour'));

        $this->app->bind(CarbonBusiness::class, function () use ($startHour, $endHour) {
            $manager = new CarbonBusiness(
                app(HolidayRepository::class)
            );
            $manager->setWorkingHoursStart($startHour[0], $startHour[1]);
            $manager->setWorkingHoursEnd($endHour[0], $endHour[1]);

            return $manager;
        });
    }

    /**
     * @return float
     */
    protected function getVersion(): float
    {
        $version = floatval($this->app->version());

        // Version does not detected
        if ($version !== 0.0) {
            return $version;
        }

        $sub = substr(
            $this->app->version(),
            strlen('lumen ('),
            6
        );

        return floatval($sub);
    }
}
