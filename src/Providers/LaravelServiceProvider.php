<?php

namespace C4uno\CarbonBusiness\Providers;

/**
 * Class CarbonBusinessServiceProvider
 * @package C4uno\CarbonBusiness
 */
class LaravelServiceProvider extends BaseServiceProvider
{

    public function boot()
    {
        $version = $this->getVersion();

        $this->publishes([
            __DIR__.'/../config/carbon-business.php' => config_path('carbon-business.php')
        ], 'config');

        if ($version >= 5.3) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        } else {
            $this->publishes([
                __DIR__.'/../database/migrations/' => database_path('migrations')
            ], 'migrations');
        }
    }
}
