<?php

declare(strict_types=1);

namespace C4uno\CarbonBusiness\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Holiday
 *
 * @property Carbon      $holiday
 * @property string|null $description
 * @package App\Models
 */
class Holiday extends Model implements HolidayInterface
{
    /**
     * @var array|string[]
     */
    protected $guarded = [];

    public $timestamps = null;

    protected $dates = [
        'holiday',
    ];
}
