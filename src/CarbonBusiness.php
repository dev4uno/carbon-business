<?php
declare(strict_types=1);

namespace C4uno\CarbonBusiness;

use C4uno\CarbonBusiness\Contracts\HolidayRepository;
use Carbon\Carbon;
use InvalidArgumentException;

class CarbonBusiness
{
    private const WORKING_HOURS_FORMAT = 'Gi';

    private HolidayRepository $holidayRepository;

    private Carbon $startTime;

    private Carbon$endTime;

    public function __construct(HolidayRepository $holidayRepository)
    {
        $this->holidayRepository = $holidayRepository;
    }

    /**
     * @param int $hour
     * @param int $minute
     */
    private static function validTime(int $hour, int $minute): void
    {
        if ($hour > 23 || $hour < 0) {
            throw new InvalidArgumentException('The start hour should be between 0 and 23');
        }

        if ($minute > 59 || $minute < 0) {
            throw new InvalidArgumentException('The start hour should be between 0 and 59');
        }
    }

    /**
     * @param int $hour
     * @param int $minute
     */
    public function setWorkingHoursStart(int $hour, int $minute): void
    {
        self::validTime($hour, $minute);

        $startTime = Carbon::now();
        $startTime->hour = $hour;
        $startTime->minute = $minute;
        $this->startTime = $startTime;
    }

    /**
     * @param int $hour
     * @param int $minute
     */
    public function setWorkingHoursEnd(int $hour, int $minute): void
    {
        self::validTime($hour, $minute);

        $endTime = Carbon::now();
        $endTime->hour = $hour;
        $endTime->minute = $minute;
        $this->endTime = $endTime;
    }

    /**
     * @return bool
     */
    public function isBusinessDay(Carbon $carbon): bool
    {
        if ($carbon->isWeekend()) {
            return false;
        }

        return !$this->holidayRepository->hasDate($carbon);
    }

    public function addBusinessDays(Carbon $carbon, int $days): Carbon
    {
        return $this->moveBusinessDays($carbon, $days, 'add');
    }

    public function subBusinessDays(Carbon $carbon, int $days): Carbon
    {
        return $this->moveBusinessDays($carbon, $days, 'sub');
    }

    /**
     * @return bool
     */
    public function inWorkingHours(Carbon $carbon): bool
    {
        $currentTime = intval($carbon->format(self::WORKING_HOURS_FORMAT));

        $assertStartTime = $currentTime >= intval($this->startTime->format(self::WORKING_HOURS_FORMAT));
        $assertEndTime = $currentTime < intval($this->endTime->format(self::WORKING_HOURS_FORMAT));

        return $assertStartTime && $assertEndTime;
    }

    /**
     * @param int    $days
     * @param string $operation Only supported 'add' or 'sub'
     * @return Carbon
     */
    private function moveBusinessDays(Carbon $carbon, int $days, string $operation): Carbon
    {
        if ($days <= 0) {
            return $carbon;
        }

        do {
            $this->moveBusinessDay($carbon, $operation);
        } while (--$days > 0);

        return $carbon;
    }

    /**
     * @param string $operation
     * @return Carbon
     */
    private function moveBusinessDay(Carbon $carbon, string $operation): Carbon
    {
        do {
            call_user_func([$carbon, $operation.'Day']);
        } while (!$this->isBusinessDay($carbon));

        return $carbon;
    }
}
