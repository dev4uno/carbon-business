<?php

declare(strict_types=1);

namespace C4uno\CarbonBusiness\Contracts;

use Carbon\Carbon;

/**
 * Class HolidayRepositoryInterface
 *
 * @package Comunidad4uno\Shared\Interfaces
 */
interface HolidayRepository
{
    /**
     * @param Carbon $date
     * @return bool
     */
    public function hasDate(Carbon $date): bool;
}
