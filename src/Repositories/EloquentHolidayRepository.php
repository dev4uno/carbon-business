<?php

declare(strict_types=1);

namespace C4uno\CarbonBusiness\Repositories;

use C4uno\CarbonBusiness\Contracts\HolidayRepository;
use C4uno\CarbonBusiness\Models\Holiday;
use Carbon\Carbon;

/**
 * Class EloquentHolidayRepository
 *
 * @package C4uno\CarbonBusiness\Repositories
 */
final class EloquentHolidayRepository implements HolidayRepository
{
    /**
     * @param Carbon $date
     * @return bool
     */
    public function hasDate(Carbon $date): bool
    {
        $number = Holiday::query()->whereDate('holiday', $date)->count();

        return $number > 0;
    }
}
