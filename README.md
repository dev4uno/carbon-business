# Carbon Business

Class to manipulate the business dates. Compatible with Laravel 5.*, 7.* and Lumen.

> The version 2 require php 7.3

## Install

```bash
composer require c4uno/carbon-business
```

### Laravel < 5.3

Add service provider to `config/app.php`

```php
C4uno\CarbonBusiness\Providers\LaravelServiceProvider::class,
```

Publish migrations and configuration

```bash
php artisan vendor:publish --provider="C4uno\CarbonBusiness\Providers\LaravelServiceProvider"
```

### Lumen

Add service provider to `bootstrap/app.php`
```php
$app->register(C4uno\CarbonBusiness\Providers\LumenServiceProvider::class);
```

#### < 5.3

Copy the migration the migration files.
If is necessary copy the configuration file manually.

## Use

```php

use C4uno\CarbonBusiness\Managers\CarbonBusiness;
use C4uno\CarbonBusiness\Contracts\HolidayRepository;
use Carbon\Carbon;

$manager = new CarbonBusiness(
    app(HolidayRepository::class)
);

// Thurday
$date = Carbon::parse('2019-02-15');
$manager->isBusinessDay($date); // true, if the date is not in the DB.
$manager->addBusinessDay($date); // '2019-02-18', by default saturday and sunday are no-business day
$manager->subBusinesDay($date); // '2019-02-18'

```


## Testing

```bash
composer run-script tests
```

## Development

You need to pass the tests `composer run-script tests` script and write test to each new feature
that you write.

